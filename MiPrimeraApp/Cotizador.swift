//
//  Cotizador.swift
//  MiPrimeraApp
//
//  Created by Kevin Belter on 11/10/16.
//  Copyright © 2016 Kevin Belter. All rights reserved.
//

import Foundation

class Cotizador {
    func obtenerValorBTC(volviConValorBitcoin: @escaping (Double?) -> ()) {
        
        func volverAlMainThreadYLlamarAlCallback(valor: Double?) {
            DispatchQueue.main.async {
                volviConValorBitcoin(valor)
            }
        }
        
        guard let url = URL(string: "http://api.coindesk.com/v1/bpi/currentprice/ars.json") else {
            volverAlMainThreadYLlamarAlCallback(valor: nil)
            return
        }
        
        let queue = DispatchQueue.global(qos: .userInitiated)
        
        queue.async {
            do {
                let data = try Data(contentsOf: url)
                
                guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
                    volverAlMainThreadYLlamarAlCallback(valor: nil)
                    return
                }
                
                guard let bpiDictionary = dictionary["bpi"] as? [String: Any] else {
                    volverAlMainThreadYLlamarAlCallback(valor: nil)
                    return
                }
                
                guard let arsDictionary = bpiDictionary["ARS"] as? [String: Any] else {
                    volverAlMainThreadYLlamarAlCallback(valor: nil)
                    return
                }
                
                guard let rate = arsDictionary["rate_float"] as? Double else {
                    volverAlMainThreadYLlamarAlCallback(valor: nil)
                    return
                }
                
                volverAlMainThreadYLlamarAlCallback(valor: rate)
            } catch {
                print(error)
                volverAlMainThreadYLlamarAlCallback(valor: nil)
            }
        }
    }
}
