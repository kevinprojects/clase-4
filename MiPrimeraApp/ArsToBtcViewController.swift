//
//  ArsToBtcViewController.swift
//  MiPrimeraApp
//
//  Created by Kevin Belter on 11/8/16.
//  Copyright © 2016 Kevin Belter. All rights reserved.
//

import UIKit

class ArsToBtcViewController: UIViewController {

    @IBOutlet weak var txtValor: UITextField!
    @IBOutlet weak var lblResultado: UILabel!
    
    @IBAction func tocoConvertir(_ sender: UIButton) {
        //Se va llamar cuando la persona toque convertir.
        
        let cotizador = Cotizador()
        cotizador.obtenerValorBTC { valorBTCOpcional in
            if let valorString = self.txtValor.text, let valor = Double(valorString), let valorBTC = valorBTCOpcional {
                let resultado =  valor / valorBTC
                self.lblResultado.text = "\(resultado)"
            }
        }
        txtValor.resignFirstResponder()
    }
}
